# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0002_operation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='argument',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='operation',
            name='biz',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='operation',
            name='celery_id',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='operation',
            name='status',
            field=models.CharField(default=b'queue', max_length=100),
        ),
        migrations.AlterField(
            model_name='operation',
            name='user',
            field=models.CharField(max_length=100),
        ),
    ]
